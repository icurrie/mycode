#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests
import time

def main():
    """your code goes below here"""
    
    while True:
        lat=input("Enter lat value must be between -90 and 90:") 
        if int(lat) in range(-90, 90):
            break

    while True:
        lon=input("Enter lon value must be between -180 and 180:")  
        if int(lon) in range(-180, 180):
            break

    URL = f"http://api.open-notify.org/iss-pass.json?lat={lat}&lon=-{64}"    
    
    req = requests.get(URL).json()

    #print(req)

    nextOver = time.ctime(req['response'][0]['risetime'])
    print(f"The next overhead pass of the ISS: {nextOver}")
    # stuck? you can always write comments
    # Try describe the steps you would take manually



if __name__ == "__main__":
    main()

