#!/usr/bin/python3

from flask import Flask
from flask import request
from flask import redirect
from flask import url_for
from flask import session
from flask import render_template

app = Flask(__name__)

app.secret_key= "random random RANDOM!"

groups = [{"hostname": "hostA","ip": "192.168.30.22", "fqdn": "hostA.localdomain"},
          {"hostname": "hostB", "ip": "192.168.30.33", "fqdn": "hostB.localdomain"},
          {"hostname": "hostC", "ip": "192.168.30.44", "fqdn": "hostC.localdomain"}]

@app.route("/", methods = ["POST", "GET"])
def index():
    #Create a Jinja2 template that would display the content of the groups value in 
    #the format shown at the top of this challenge.
    
    #return render_template("groups.html", string = s)

    if request.method == "POST":
        hostname = request.form.get("hn")
        ip = request.form.get("ip")
        fqdn = request.form.get("fqdn")
        groups.append({"hostname":hostname, "ip":ip, "fqdn":fqdn})

    s = ""
    for i in groups:
        s += f"{i['ip']} {i['hostname']}\n"
    
    return render_template("groups.html", string = s)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224)
