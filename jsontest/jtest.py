import requests
import json

URL_date = "http://date.jsontest.com"
URL_ip = "http://ip.jsontest.com/"
URL_valid = "http://validate.jsontest.com/"

resp_time = requests.get(URL_date).json()
resp_ip = requests.get(URL_ip).json()

print(resp_time)
print(resp_ip)

with open("myservers.txt", "r") as sf:
    resp_servers = sf.read()

print(json.dumps(resp_servers))

mydata = {"json": f"time:{resp_time['date']},ip:{resp_ip['ip']}"}

print(mydata)

